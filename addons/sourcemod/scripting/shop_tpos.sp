//#include <shop>
#include <sdktools>
#include <sdkhooks>
#include <devzones>
#pragma newdecls required
#include <countts>
#include <sm_jail_redie>
#pragma semicolon 1

#define VERSION "1.7.200"
stock void debugMessage(const char[] message, any ...)
{
	char szMessage[256], szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/debug_shop_tpos.txt");
	VFormat(szMessage, sizeof(szMessage), message, 2);
	LogToFile(szPath, szMessage);
}
#define dbgMsg(%0) debugMessage(%0)

int g_iEnt[MAXPLAYERS+1]	= {-1, ...};		// ентити бочек
int g_iLaserSprite			= -1;
int g_iLaserHalo			= -1;
int g_iCT					= -1;				// Кто запустил запрос, !(-1) - значит запрос в процессе
bool g_bInJail[MAXPLAYERS+1];
bool g_bRndstart;
float g_fTpos[MAXPLAYERS+1][3];

//timers
Handle h_Timer_EnablePos;
Handle h_Timer_CheckJails;
Handle h_Chat5;
Handle h_Chat10;
Handle h_Chat15;
Handle h_Chat20;
Handle h_Timer_TestPos;
Handle h_Force_transmit_ents = null;

public Plugin myinfo =
{
	name		= "[Shop] T pos",
	author		= "ShaRen",
	description	= "Определяет местоположение Т",
	version		= VERSION
}

Handle Sv_force_transmit_ents	= null;

public void OnPluginStart()
{
	Sv_force_transmit_ents = FindConVar("sv_force_transmit_ents");
	RegConsoleCmd("sm_tpos", Command_TestPos, "Get T pos");
	RegConsoleCmd("sm_tpos_l", Command_TestPos_l, "Get T pos list");
	RegConsoleCmd("sm_dpos", CommandDelSpray);
	HookEvent("round_start",		OnRoundStart);
	HookEvent("round_end",			OnRoundEnd);
	HookEvent("player_death",		OnPlayerDeath);
	HookEvent("player_disconnect",	OnPlayerDeath, EventHookMode_Pre);
	//HookEvent("player_team",		OnPlayerDeath);
}

public void OnPluginEnd()
{
	DelSpray();
}

public void OnMapStart()
{
	g_iLaserSprite	= PrecacheModel("materials/sprites/arrow.vmt");
	g_iLaserHalo	= PrecacheModel("materials/sprites/light_glow02.vmt");
	PrecacheModel("models/props/de_train/hr_t/barrel_a/barrel_a.mdl");
	//dbgMsg("OnMapStart");
}

public	Action CommandDelSpray(int client, int args)
{
	if (IsValidCT(client)) {
		DelSpray();
		g_iCT = -1;
	} else PrintToChat(client, "Только живые охранники могут удалять маркеры");
	
	//dbgMsg("Action CommandDelSpray");
	
	return Plugin_Handled;
}

public void OnPlayerDeath(Event e, const char[] name, bool dontBroadcast)
{	// death or disconnect
	int client = GetClientOfUserId(e.GetInt("userid"));
	if (client && IsClientInGame(client) && GetClientTeam(client) == 2) {
		dbgMsg("DelSpray OnPlayerDeath/disconnect (noCT) %L", client);
		DelSpray(client);		// удаляем спрей Т т.к. он умер
	}
	if (g_iCT != -1) {			// если идет запрос
		bool b;		// есть ли сбежавшие?
		for(int i=1; i<MAXPLAYERS; i++)
			if(IsValidT(i) && IsTRunner(i)) {
				b = true;
				break;
			}
		if (!b) {										// если нет сбежавших, то останавливает запрос !tpos
			for(int i=1; i<MAXPLAYERS; i++)
				if(IsValidCT(i)) {
					PrintToChat(i,"Запрос игрока %N был отменен т.к. все сбежавжие мертвы.", g_iCT);
					PrintToChat(i,"Если это не так, то пересчитайте (!check) заключенных и заного отправьте запрос (!tpos).");
				}
			g_iCT = -1;
			dbgMsg("DelSpray() OnPlayerDeath");
			DelSpray();
		}
	} else if (client == g_iCT) {
		dbgMsg("DelSpray OnPlayerDeath (g_iCT) %L", client);
		DelSpray();
		g_iCT = -1;
	}
}

public void t2lrcfg_OnLrStarted()
{
	dbgMsg("DelSpray() t2lrcfg_OnLrStarted");
	DelSpray();
}

//public void countts_check()
//{
//	Timer_CheckJails(null);
//}

public void everyone_is_here_every()	// countts когда всех собирают
{
	dbgMsg("DelSpray() everyone_is_here_every");
	DelSpray();
}

void DelSpray(int client=0)
{
	if (!client) {							// для всех
		for(int i=1; i<MAXPLAYERS; i++)
			if (IsValidT(i)) {
				if (g_iEnt[i] != -1) {
					if (IsValidEntity(g_iEnt[i])) {
						dbgMsg("(в цикле) RemoveEdict(g_iEnt[%L] (%i))", i, i);
						RemoveEdict(g_iEnt[i]);
						g_iEnt[i] = -1;
					} else dbgMsg("(в цикле) !IsValidEntity(g_iEnt[%i]) == %i", i, g_iEnt[i]);
				} else dbgMsg("(в цикле) g_iEnt[%i] == -1", i);
			}// else dbgMsg("(в цикле) void DelSpray %i !IsValidT", i);
		g_iCT = -1;
		StopTPosTimers();														// останавливает таймеры вызванные командой !tpos
	} else if (IsValidEntity(g_iEnt[client])) {
		if (g_iEnt[client] != -1) {			// когда 1 Т умер
			dbgMsg("RemoveEdict(g_iEnt[%L]) (%i)", client, client);
			RemoveEdict(g_iEnt[client]);
			g_iEnt[client] = -1;
		} else dbgMsg("g_iEnt[%i] == -1", client);
	} else dbgMsg("DelSpray Впустую %i ( g_iEnt[client] is invalid entity)", client);
}

public void OnRoundEnd(Event event, const char[] name, bool dontBroadcast)
{
	DelSpray();
}

public void OnRoundStart(Event event, const char[] name, bool dontBroadcast)
{
	DelSpray();
	g_iCT = -1;
	dbgMsg("OnRoundStart");
	StopTPosTimers();												// останавливает таймеры вызванные командой !tpos
	
	delete h_Force_transmit_ents;
	delete h_Timer_EnablePos;										// Таймер с начала раунда
	delete h_Timer_CheckJails;
	
	h_Timer_EnablePos = CreateTimer(50.0, Timer_EnablePos);			// время с начала раунда когда функцией нельзя ползоваться
	h_Timer_CheckJails = CreateTimer(70.0, Timer_CheckJails);		// время с начала раунда когда Т в джайлах будут подсвечены
	
	g_bRndstart = true;
	for (int i=1; i<MAXPLAYERS; i++)
		g_bInJail[i] = false;
}

public void StopTPosTimers()
{
	delete h_Timer_TestPos;
	delete h_Chat20;
	delete h_Chat15;
	delete h_Chat10;
	delete h_Chat5;
}

public Action Command_TestPos_l(int client, int args)
{
	for(int i=1; i<MAXPLAYERS; i++) {
		if(IsClientInGame(i)) {
			PrintToConsole(client, "g_iEnt[%i] = %i (%s) %s %s %N ", i, g_iEnt[i], IsValidEntity(g_iEnt[i])?"Valid":"InValid", IsTRunner(i)?"Runner":"", g_bInJail[i]?"InJail":"", i);
		} else PrintToConsole(client, "g_iEnt[%i] = %i (%s) %s %s", i, g_iEnt[i], IsValidEntity(g_iEnt[i])?"Valid":"InValid", IsTRunner(i)?"Runner":"", g_bInJail[i]?"InJail":"");
	}
}

public Action Command_TestPos(int client, int args)
{
	int rebels;
	for(int i=1; i<MAXPLAYERS; i++)
		if(IsValidT(i) && IsTRunner(i))
			rebels++;
	if (!IsValidCT(client))				PrintToChat(client, "Только живые охранники могут пользоваться этой функцией");
	else if (g_iCT != -1)				PrintToChat(client, "Отказано, один запрос уже в процессе!");
	else if (g_bRndstart)				PrintToChat(client, "Отказано, не могу сканировать территори 50 сек с начала раунда.");
	else if (!TPosAvailable())			PrintToChat(client, "Сначала обновите информацию о сбежавших командой !check");
	else if (rebels > 0) {
		g_iCT = client;
		PrintToChat(client, "Посылаю запрос на координаты %i заключенных ...", rebels);
		dbgMsg("Посылаю запрос на координаты заключенных");
		h_Chat5			= CreateTimer(5.0,  Chat5,  client);
		h_Chat10		= CreateTimer(10.0, Chat10, client);
		h_Chat15		= CreateTimer(15.0, Chat15, client);
		h_Chat20		= CreateTimer(20.0, Chat20, client);
		h_Timer_TestPos = CreateTimer(25.0, Timer_TestPos, client);
		//h_Timer_TestPos = CreateTimer(1.0, Timer_TestPos, client);
	} else if (rebels == 0)				PrintToChat(client, "Сбежавших не зафиксировано, если это не так, то пересчитай заключенных (!check)");
}

stock void playmusic(char[] sound)
{
	for(int i=1; i<MAXPLAYERS; i++)
		if(IsClientInGame(i) && !IsFakeClient(i))
			ClientCommand(i, sound);
}

public Action Timer_CheckJails(Handle timer)
{
	h_Timer_CheckJails = null;
	bool bInJ = false;
	for (int i=1; i<MAXPLAYERS; i++) {			// Удаляем метки тех кто был в дж 7 сек назад, и ставим новые 
		dbgMsg("DelSpray(%i) Timer_CheckJails", i);
		if (g_bInJail[i]) {
			g_bInJail[i] = false;				// т.к. сейчас будет перепроверять по новой
			DelSpray(i);						// если он до этого был помечен в джайле
		}
		if (IsValidT(i) && Zone_IsClientInZone(i, "jail", false)) {
			DelSpray(i);						// перед CreateEntityByName
			g_iEnt[i] = CreateEntityByName("prop_dynamic_glow");
			if (g_iEnt[i] != -1) {
				float fPos[3];
				GetClientAbsOrigin(i, fPos);
				g_bInJail[i] = true;
				bInJ = true;
				MarkT(i, fPos);
				dbgMsg("Timer_CheckJails MarkT %L", i);
			}
		}
	}
	if (bInJ)		// остался ли ещё кто в джайлах
		h_Timer_CheckJails = CreateTimer(7.0, Timer_CheckJails);
}

public Action Timer_EnablePos(Handle timer)
{
	h_Timer_EnablePos = null;
	dbgMsg("g_bRndstart to false");
	g_bRndstart = false;									// по истечении n сек с начала раунда
}

public Action Chat5(Handle timer, any client)
{
	h_Chat5 = null;
	if (IsValidCT(client))
		PrintToChat(client, "Запрос принят, обработка ...");
	playmusic("play ambient/office/tech_oneshot_06.wav");
}

public Action Chat10(Handle timer, any client)
{
	h_Chat10 = null;
	if (IsValidCT(client))
		PrintToChat(client, "Сканирую территорию ...");
	playmusic("play ambient/office/tech_oneshot_07.wav");
}

public Action Chat15(Handle timer, any client)
{
	h_Chat15 = null;
	PrintToChatAll("Внимание! Один из охраников послал запрос на координаты заключенных.");
	if (IsValidCT(client))
		PrintToChat(client, "Обработка и запись координат ...");
	for(int i=1; i<MAXPLAYERS; i++)
		if (IsValidT(i) && IsTRunner(i))
			GetClientAbsOrigin(i, g_fTpos[i]);
		else g_fTpos[i] = view_as<float>({0.0, 0.0, 0.0});
	playmusic("play ambient/office/tech_oneshot_08.wav");
}

public Action Chat20(Handle timer, any client)
{
	h_Chat20 = null;
	if (IsValidCT(client))
		PrintToChat(client, "Отправка координат ...");
	playmusic("play ambient/office/tech_oneshot_09.wav");
}

public Action Timer_TestPos(Handle timer, any client)
{
	h_Timer_TestPos = null;
	PrintToChatAll("На всех заключенных установлены маячки, для поиска кемперящих заключенных");
	if (IsValidCT(client))
		PrintToChat(client, "Данные получены. Линии удалятся через 15 секунд.");
	
	dbgMsg("DelSpray()  %L Timer_TestPos", client);
	DelSpray();
	g_iCT = -1;
	for(int i=1; i<MAXPLAYERS; i++) {
		//g_iEnt[i] = CreateEntityByName("prop_dynamic");
		if (IsValidT(i) && IsTRunner(i)) {
			g_iEnt[i] = CreateEntityByName("prop_dynamic_glow");
			if (g_iEnt[i] != -1) {
				//float pos[3];
				float posCT[3];
				if (IsValidCT(client))
					GetClientAbsOrigin(client, posCT);
				if (g_fTpos[i][0] == 0.0 && g_fTpos[i][1] == 0.0 && g_fTpos[i][2] == 0.0)
					GetClientAbsOrigin(i, g_fTpos[i]);
				//pos[2] += 50.0;
				g_bInJail[i] = false;
				MarkT(i, g_fTpos[i]);
				dbgMsg("MarkT Timer_TestPos %L", i);
				
				if (IsValidCT(client)) {
					TE_SetupBeamPoints(g_fTpos[i], posCT, g_iLaserSprite, g_iLaserHalo, 0, 0, 15.0, 7.0, 0.0, 0, 0.0, {100, 100, 100, 255}, 40);
					TE_SendToClient(client);
				}
				//PrintToChatAll("%N(%i) %.1f %.1f %.1f test_tpos %s", i, i, g_fTpos[i][0], g_fTpos[i][1], g_fTpos[i][2], buffer);
			}
		}
	}
}

void MarkT(int i, float fPos[3])
{
	char buffer[256];
	Format(buffer, sizeof(buffer), "Tpos_model%d", i);
	DispatchKeyValue(i,		  	"targetname",				buffer);
	DispatchKeyValue(g_iEnt[i], "model",				"models/props/de_train/hr_t/barrel_a/barrel_a.mdl");
	DispatchKeyValue(g_iEnt[i], "disableflashlight",		"1");
	DispatchKeyValue(g_iEnt[i], "disablereceiveshadows",	"1");
	DispatchKeyValue(g_iEnt[i], "disableshadowdepth",		"1");
	DispatchKeyValue(g_iEnt[i], "disableshadows",			"1");
	DispatchKeyValue(g_iEnt[i], "solid",					"0");
	DispatchKeyValue(g_iEnt[i], "rendermode",				"1");
	DispatchKeyValue(g_iEnt[i], "fademindist",				"-1");
	DispatchKeyValue(g_iEnt[i], "glowdist",					"65535");
	DispatchKeyValue(g_iEnt[i], "glowenabled",				"1");
	DispatchKeyValue(g_iEnt[i], "glowcolor",				"255 0 0");
	DispatchKeyValue(g_iEnt[i], "renderamt",				"1");		// прозрачность
	DispatchSpawn(g_iEnt[i]);
	TeleportEntity(g_iEnt[i], fPos, NULL_VECTOR, NULL_VECTOR);
	//PrintToChatAll("%f %f %f", fPos[0], fPos[1], fPos[2]);
	SetConVarBool(Sv_force_transmit_ents, true);
	h_Force_transmit_ents = CreateTimer(0.5, t_Force_transmit_ents);
}

public Action t_Force_transmit_ents(Handle timer, any client)
{
	h_Force_transmit_ents = null;
	SetConVarBool(Sv_force_transmit_ents, false);
}

stock bool IsValidT(int client)
{
	if (!IsClientInGame(client) || !IsPlayerAlive(client) || IsPlayerGhost(client) || GetClientTeam(client) != 2 )// || IsFakeClient(client)
	//if (IsClientInGame(client) || !IsPlayerAlive(client) || IsPlayerGhost(client) || GetClientTeam(client) != 2 || IsFakeClient(client))
		return false;
	return true;
}

stock bool IsValidCT(int client)
{
	//if (!IsClientInGame(client)) // || !IsPlayerAlive(client)|| IsPlayerGhost(client) || GetClientTeam(client) != 3 || IsFakeClient(client)
	if (!IsClientInGame(client) || !IsPlayerAlive(client) || IsPlayerGhost(client) || GetClientTeam(client) != 3 || IsFakeClient(client))
		return false;
	return true;
}